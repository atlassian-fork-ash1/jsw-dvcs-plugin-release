# DVCS Plugin release

* For Jira Software 7.6 to 7.13 versions, plugin release is available in folder jws_7_6_7_13
* For Jira Software 8.0 to 8.2 versions, plugin release is available in folder jsw_8_0_8_2
* For Jira Software 8.3 version onwards, no plugin update is needed
